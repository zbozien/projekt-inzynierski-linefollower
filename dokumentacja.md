# Podstawowe biblioteki do działania programu

Do odpowiedniego działania algorytmu potrzebujemy różnych bibliotek tj. **numpy**, **imutils** czy **OpenCV**.
Dodatkowo importujemy bibliotekę **time** która jest potrzebna do przeliczania ilości klatek na sekundę, które będziemy przetwarzać.

```python
from controller import Robot
import numpy as np
import cv2
from controller import Camera
import imutils
import time
```

# Inicjalizacja napędu

Z biblioteki **controller** możemy użyć metodę **Robot()**.
Następnie inicjalizujemy podstawowy napęd w naszym robocie - prawy i lewy silnik krokowy przy pomocy metody **.getMotor()**
kolejnym krokiem jest przypisanie wartości przy pomocy funkcji **.setPosition()** na wartość +inf (plus nieskończoność) w celu możliwości sterowania prędkością zamiast ilością obrotów.
Ustalamy maksymalną prędkość w zmiennej **maxSpeed**.

```python
robot = Robot()
timestep = int(robot.getBasicTimeStep())

leftMotor = robot.getMotor('left wheel motor')
rightMotor = robot.getMotor('right wheel motor')

leftMotor.setPosition(float('+inf'))
rightMotor.setPosition(float('+inf'))
maxSpeed = min(rightMotor.getMaxVelocity(), leftMotor.getMaxVelocity())
```

# Inicjalizacja kamery pokładowej

```python
cam = robot.getCamera('camera')
cam.enable(1)
```

# Sterowanie

Zadeklarowana klasa przyjmuje 3 argumenty - **angle**, **speed**, **steering**.

 * angle - kąt nachylenia
 * speed - maksymalna prędkość
 * steering - wartość liczbowa wypadkowej równania zmiennych **(error*kp)+(ang*ap)**.

```python
def Motor_steer (angle, speed, steering):
```
Funkcja składa się z trzech instrukcji warunkowych, które sprawdzają zmienną **steering**. 

> # Na wprost

Jeżeli zmienna **steering** jest równa 0, robot porusza się z maksymalną prędkością

```python
if steering == 0 :
        leftMotor.setVelocity(speed)
        rightMotor.setVelocity(speed)
        return
```
> # W Prawo

W przypadku gdy zmienna **steering** jest większa od 0, obydwa motory otrzymują procentowo pomniejszoną prędkość na podstawie zmiennej **angle** a dodatkowo prędkość prawego motoru jest pomniejszona o procentową wartość zmiennej **steering**.

```python
 elif steering > 0:
        steering = 100 - steering
        steering = 100 - steering
        if angle<0:
            angle=angle*-1
        angle=(angle*100)/180
        angle=angle*aap
        angle=100-angle     
        leftMotor.setVelocity(speed*angle/100)
        rightMotor.setVelocity((speed*angle/100)*steering/100)        
        return
```
> # W Lewo

Analogiczna sytuacja odbywa się kiedy zmienna **steering** jest mniejsza od 0, obydwa motory mają pomniejszoną prędkość i dodatkowo prędkość lewego motoru jest zmniejszona.

```python
 elif steering < 0:
        steering = steering * -1        
        steering = 100 - steering
        if angle<0:
            angle=angle*-1
        angle=(angle*100)/180
        angle=angle*aap 
        angle=100-angle
        x=speed*angle/100
        leftMotor.setVelocity((speed*angle/100)*steering/100)
        rightMotor.setVelocity(speed*angle/100 )     
        return
```

# Pętla Nieskończona

Robot działa w pętli **while**, która jest pętlą nieskończoną, aby robot jeździł po mapie cały czas.
Warunkiem zakończenia pętli jest pojawienie się wartości -1.

```python
while robot.step(timestep) != -1:
```

# Wczytywanie klatki z kamery pokładowej

Do zmiennej **temp** przypisujemy tablicę uzyskaną z metody **.getImageArray()**.
Następnie przy pomocy **np.array** przetwarzamy tablicę w liste.
Kolejnym krokiem jest obrócenie obrazu o 90 stopni w kierunku przeciwnym do wskazówek zegara przy pomocy biblioteki **imutils**.
Odbicie lustrzane uzyskujemy dzięki bibliotece OpenCV po przez zastosowanie metody **.flip()**
Ostatnim krokiem jest wycięcie pożądanego obszaru obrazu.

```python
temp = cam.getImageArray()
    frame1 = np.array(temp, dtype=np.uint8)    
    frame1 = imutils.rotate(frame1,-90)    
    frame = cv2.flip(frame1, 1)
    frame = frame[20:90,0:119]
```

# Znajdowanie linii oraz konturu

W  tym  obszarze  kodu  wykorzystujemy  metodę  biblioteki OpenCV - **inRange()** -  Ma  ona  za zadanie przetwarzać barwy w zakresie 0-150 (w systemie RGB) jako czarny.
Następnie na końcach czarnej linii rysowany jest kontur, dzięki któremu będziemy w przyszłości obliczać kąt oraz błąd odchylenia.
Jeżeli znajdzie kontur, rysuje wokół niego kwadrat (informacja dla programistów, że algorytm się dobrze wykonuje).



```python
Blackline = cv2.inRange(frame, (0,0,0), (110,110,110))
    contours, hierarchy = cv2.findContours(Blackline.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(frame, contours,-1,(0,255,0), 3)
    
    if len(contours)>0:    
        x,y,w,h = cv2.boundingRect(contours[0])  
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
        
        blackbox = cv2.minAreaRect(contours[0])
```

# Przeliczanie kąta oraz błędu 

Przy pomocy funkcji **minAreaRect** otrzymujemy wartość liczbową nachylenia konturu, którą przeliczamy  później  przy  pomocy  instrukcji  warunkowych  aby  uzyskać  kąt  w  stopniach.
Następne  jest przeliczenie błędu na podstawie odsunięcia konturu od środka obrazu.
W zmiennej **setpoint** ustalamy środek obrazu jako wartość na osi X.
**error** jest różnicą zmiennej **x_min** (maksymalnie wysunięta krawędź konturu linii) i zmiennej **setpoint**.

```python
   (x_min,y_min), (w_min,h_min), ang = blackbox        
        if ang <-45:
            ang = 90+ang
        if w_min < h_min and ang > 0:
            ang = (90-ang)*-1
        if w_min > h_min and ang < 0:
            ang = 90 + ang
            
        setpoint = 60       
        error = int(x_min - setpoint)
        ang = int(ang)
```

# Inicjalizacja sterowania

Wywołujemy funkcje sterującą podając argumenty tj.

* ang - kąt nachylenia konturu.
* maxSpeed - maksymalna prędkość.
* error - błąd odsunięcia konturu od środka obrazu.
* kp - modyfikator procentowy błędu (Zwiększając tą wartość, robot zwiększy procentowo kąt skrętu w zależności od wielkości błędu).
* ap - modyfikator procentowy kąta (Zwiększając tą wartość, robot zwiększy procentowo kąt skrętu w zależności od wielkości kąta).

```python
Motor_steer(ang,maxSpeed,(error*kp)+(ang*ap))        
```

# Wyświetlanie informacji na ekranie

* **cv2.drawContours(frame, [box],0,(0,0,255),2)** rysuje wokół wykrytego konturu zieloną linię.
* **cv2.putText(frame,str(ang),(25,20),cv2.FONT_HERSHEY_COMPLEX, 0.5, (255,0,0), 1)** wyświetla wartość liczbową kąta.
* **cv2.line(frame, (int(x_min), 40), (int(x_min), 60), (255,0,0), 3)** niebieska linia wskazująca środek konturu.
* **centertext = "error = "+ str(error)** - zmienna przechowująca błąd w formacie **string**
* **cv2.putText(frame, centertext, (5,25), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255,0,255), 1)** wyświetla błąd opisany linijke powyżej.

```python
box = cv2.boxPoints(blackbox)
        box = np.int0(box)
                
        cv2.drawContours(frame, [box],0,(0,0,255),2)
        cv2.putText(frame,str(ang),(25,20),cv2.FONT_HERSHEY_COMPLEX, 0.5, (255,0,0), 1)
        
            
        cv2.line(frame, (int(x_min), 40), (int(x_min), 60), (255,0,0), 3) 
        centertext = "error = "+ str(error)
        cv2.putText(frame, centertext, (5,25), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255,0,255), 1)
```

# Wyświetlanie obrazu

Przy użyciu **cv2.imshow()** wyświetlany jest obraz pojedynczej klatki.
Warunkiem zakończenia wyświetlania jest wciśnięcie klawisza **'q'**.

```python
cv2.imshow('frame',frame)
    cv2.imshow('black',Blackline)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
```

# Wyświetlanie ilości klatek na sekundę

Do obliczenia ilości przetwarzanych klatek na sekundę potrzebujemy zmiennych **start_time**, która jest umieszczona na początku algorytmu, zmiennej **finish_time** oraz zmiennej **counter**, która
jest zwiększana o 1 po każdym obrocie pętli.

```python
start_time=time.time()
finish_time=time.time()
fps=counter/(finish_time-start_time)
print("fps: " + str(fps))
```