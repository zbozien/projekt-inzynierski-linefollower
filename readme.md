
# Adaptacyjny LineFollower

LineFollower korzystający z OpenCV, Python w środowisku Webots.

Dzięki temu algorytmowi, robot potrafi zmniejszych/zwiększyć prędkość w zależności od drogi (prosta droga, zakręty) i osiąga lepszy czas na trasie niż tradycyjny algorytm LineFollower. 


# Demonstracja

[![PORÓWNANIE ALGORYTMÓW - LINEFOLLOWER](http://img.youtube.com/vi/_hjp4mt3OGo/0.jpg)](http://www.youtube.com/watch?v=_hjp4mt3OGo "Porównanie algorytmów")

---

# Wymagania

Niżej opisane są wersje, na których tworzyliśmy algorytm - możliwe, że zadziała również na wersjach wcześniejszych!

* Webots R2020A Revision 1
* Python 3.7
* OpenCV 4.0.0.21
* numpy 1.16.1
* imutils 0.5.2

---

# Jak uruchomić?

* Po zainstalowaniu wszystkich potrzebnych modułów opisanych wyżej włącz środowisko Webots.
* Utwórz nowy projekt.
* Utwórz nowy kontroler, wybierz język programowania Python.
* Zaimportuj/skopiuj kod z pliku line_follower.py
* Utwórz odpowiednią trasę/arenę - nie przekraczaj grubości linii 50px - linia powinna być czarna.
* Dodaj robota e-puck i ustaw następującą konfigurację:
* ![Ustawienie-robota-epuck](img/Ustawienie-robota-epuck.png)
* Uruchom symulacje:
* ![Symulacja](img/Symulacja.png)

---

# Twórcy

* Sebastian Zbozien <sebastian.zbozien@student.up.krakow.pl>
* Konrad Rygielski <konrad.rygielski@student.up.krakow.pl>

---

# Dokumentacja

Pełna dokumentacja dostępna jest w pliku [dokumentacja.md](https://bitbucket.org/zbozien/projekt-inzynierski-linefollower/src/master/dokumentacja.md).