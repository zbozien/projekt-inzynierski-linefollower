from controller import Robot
import numpy as np
import cv2
from controller import Camera
import imutils
import time

robot = Robot()

timestep = int(robot.getBasicTimeStep())

#inicjalizacja napędu
leftMotor = robot.getMotor('left wheel motor')
rightMotor = robot.getMotor('right wheel motor')

leftMotor.setPosition(float('+inf'))
rightMotor.setPosition(float('+inf'))
maxSpeed = min(rightMotor.getMaxVelocity(), leftMotor.getMaxVelocity())


#inicjalizacja kamery
cam = robot.getCamera('camera')
cam.enable(1)
#modyfikator reduktora prędkości
aap = 1.3
#sterowanie
def Motor_steer (angle, speed, steering):
    if steering == 0 :
        leftMotor.setVelocity(speed)
        rightMotor.setVelocity(speed)
        return
    elif steering > 0:
        steering = 100 - steering
        if angle<0:
            angle=angle*-1
        angle=(angle*100)/180
        angle=angle*aap
        angle=100-angle     
        leftMotor.setVelocity(speed*angle/100)
        rightMotor.setVelocity((speed*angle/100)*steering/100)
        
        return
    elif steering < 0:
        steering = steering * -1        
        steering = 100 - steering
        if angle<0:
            angle=angle*-1
        angle=(angle*100)/180
        angle=angle*aap 
        angle=100-angle
        x=speed*angle/100
        leftMotor.setVelocity((speed*angle/100)*steering/100)
        rightMotor.setVelocity(speed*angle/100 )     
        return
#modyfikator błędu     
kp= .9
#modyfikator kąta
ap = 1

start_time=time.time()
counter=0
while robot.step(timestep) != -1:
    counter+=1
#wczytywanie klatki z kamery
    temp = cam.getImageArray()
    frame1 = np.array(temp, dtype=np.uint8)    
    frame1 = imutils.rotate(frame1,-90)    
    frame = cv2.flip(frame1, 1)
    frame = frame[20:90,0:119]
    
    
#znajdywanie linii oraz konturu
    Blackline = cv2.inRange(frame, (0,0,0), (150,150,150))
    contours, hierarchy = cv2.findContours(Blackline.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(frame, contours,-1,(0,255,0), 3)
    
    if len(contours)>0:    
        x,y,w,h = cv2.boundingRect(contours[0])  
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
#przelicznie kąta        
        blackbox = cv2.minAreaRect(contours[0])
        (x_min,y_min), (w_min,h_min), ang = blackbox        
        if ang <-45:
            ang = 90+ang
        if w_min < h_min and ang > 0:
            ang = (90-ang)*-1
        if w_min > h_min and ang < 0:
            ang = 90 + ang
            
        setpoint = 60
#przeliczanie błędu       
        error = int(x_min - setpoint)
        ang = int(ang)
#inicjalizacja sterowania
        Motor_steer(ang,maxSpeed,(error*kp)+(ang*ap))        
        box = cv2.boxPoints(blackbox)
        box = np.int0(box)
                
        cv2.drawContours(frame, [box],0,(0,0,255),2)
        cv2.putText(frame,str(ang),(25,20),cv2.FONT_HERSHEY_COMPLEX, 0.5, (255,0,0), 1)
        
            
        cv2.line(frame, (int(x_min), 40), (int(x_min), 60), (255,0,0), 3) 
#wyświetlanie błędu 
        centertext = "error = "+ str(error)
        cv2.putText(frame, centertext, (5,35), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255,0,255), 1)
    
 
#wyświetlanie obrazu   
    
    cv2.imshow('frame',frame)
    cv2.imshow('black',Blackline)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
#ilość przetwarzanych klatek na sekundę
finish_time=time.time()
fps=counter/(finish_time-start_time)
print("fps: " + str(fps))
